# README #

The following are instructions for setting up gulp-sass for a project.
This can be used for either static builds or framework projects.

### Commands ###

* npm init      (This will create the package.json file and will allow you to populate parts of it.)

Note: You can keep pressing 'Enter' and skip through the entries.

* npm install -g gulp     (This will globally install gulp -- ONLY IF YOU NEED TO!)

* npm install --save-dev gulp     (Install gulp in your project and add it to the Dependencies in package.json)
* npm install --save-dev gulp-sass      (Install gulp-sass, same way as above^^)
* Fill in the gulpfile.js to match the one in this project. Remember to Change the filepaths to suit your project.
* Done, when making changes run gulp and it will automatically compile the sass into css.


### Alternatively ###
* Download this project
* Run npm install
* Modify the project to fit your needs, don't forget about changing the file paths in gulpfile.js if needs be.
* Done, when making changes run gulp and it will automatically compile the sass into css.
